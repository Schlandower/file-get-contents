# file-get-contents

PHP style file reader, reads entire file into a string.
This module automatically detects file encoding.

Example usage:
var fileGetContents = require('@schlandower/file-get-contents');
var fc = fileGetContents('path/to/your/file');