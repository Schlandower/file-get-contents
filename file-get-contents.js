var fs = require('fs');

//module.exports = function (filename, encoding = 'utf8') {
module.exports = function (filename) {
	try {
		//return fs.readFileSync(filename, encoding);
		var buffer = fs.readFileSync(filename);
		return buffer.toString();
	} catch(e) {
		throw e;
	}
};